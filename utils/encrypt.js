const bcrypt = require("bcrypt");
const SALT_ROUNDS = 3;
require("custom-env").env(process.env.NODE_ENV);

const hashPlainText = plainText =>
  process.env.START_HASH + plainText + process.env.FINISH_HASH;
const compare = (plainText, hashedText) =>
  bcrypt.compareSync(hashPlainText(plainText), hashedText);
const encrypt = plainText =>
  bcrypt.hashSync(hashPlainText(plainText), SALT_ROUNDS);

module.exports = {
  encrypt,
  compare
};
