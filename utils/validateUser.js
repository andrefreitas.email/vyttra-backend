const User = require('../models/user');
const Admin = require('../models/admin');

const validateUser = async (req, res, next) => {
  const { email, login } = req.headers;
  if (!email || !login)
    return res.send({
      success: false,
      message: 'Email or login not received',
      code: 'missingPassOrLog'
    });

  const isUserReq = await User.find({ email, login }).limit(1);
  const isAdmiReq = await Admin.find({ email, login }).limit(1);

  if (!isUserReq.length && !isAdmiReq.length)
    return res.send({
      success: false,
      message: 'You must be logged to complete this action',
      code: 'expiredLogin'
    });
  return next();
};
module.exports = async (req, res, next) => {
  validateUser(req, res, next);
};
