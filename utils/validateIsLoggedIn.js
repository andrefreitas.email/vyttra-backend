module.exports = async (req, res, next) => {
  const { email, password } = req.body;
  if (!email || !password)
    res
      .status(412)
      .send({
        success: false,
        message: 'password or email not received',
        code: 'missingPassOrEmail'
      });

  return next();
};
