const Admin = require('../models/admin');

const validateAdmin = async (req, res, next) => {
  const { email, login } = req.headers;

  if (!email || !login)
    return res.send({
      success: false,
      message: 'Admin tokens not received',
      code: 'missingPassOrLog',
    });

  const isLogged = await Admin.find({ email: email, login: login }).limit(1);

  if (!isLogged.length)
    return res.send({
      success: false,
      message: 'You must be logged to complete this action',
      code: 'expiredLogin',
    });

  return next();
};
module.exports = async (req, res, next) => {
  validateAdmin(req, res, next);
};
