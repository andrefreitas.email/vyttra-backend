const sgMail = require('@sendgrid/mail');
require('custom-env').env(process.env.NODE_ENV);
const DEFAULT_SUBJECT = 'Vyttra contact email';
const DEFAULT_FROM = 'contato@vyttra.com';

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

/**
 * Global fn to send emails
 *
 * @param {String} - destiny
 * @param {String} - subject
 * @param {String} - text to be sent
 */
const sendEmail = ({ to, subject = DEFAULT_SUBJECT, text }) => {
  return sgMail.send(
    {
      to,
      from: DEFAULT_FROM,
      subject,
      text
    },
    false,
    (error, result) => {
      if (error) throw new Error(error);
    }
  );
};

module.exports = sendEmail;
