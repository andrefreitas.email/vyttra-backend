const Product = require("../models/produtoFinal");
const File = require("../models/files");
const updateProductsWithSegment = ({ linhaId, productList, amaisList }) => {
  const promisifiedUpdate = productList.map(productId =>
    Product.findByIdAndUpdate({ _id: productId }, { linha: linhaId })
  );
  console.log(123, amaisList);

  const promisifiedUpdateAmais = amaisList.map(productId =>
    File.findByIdAndUpdate({ _id: productId }, { linha: linhaId })
  );

  return Promise.all([...promisifiedUpdate, ...promisifiedUpdateAmais]);
};
module.exports = updateProductsWithSegment;
