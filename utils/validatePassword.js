const PASSWORD_REGEX = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/

const testPassword = password => PASSWORD_REGEX.test(password)

module.exports = (password, res) => {
  const shouldPass = testPassword(password)
  if (!shouldPass && res) res.send({ success: false, message: 'Password should be +8 digits and have uppercase + lowercase + number + simbol', code: 'passwordNotMatchRules' })
  return shouldPass
}
