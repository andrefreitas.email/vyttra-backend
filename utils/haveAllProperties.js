/**
 *  Function to test properties of object w a list of properties that will be present there
 *
 * @param {Object} object - obj to be tested
 * @param {[String]} array - list of properties that will be tested
 * @returns Boolean
 */
module.exports = (object, array, res) => {
  const shouldPass = array.every(prop => object.hasOwnProperty(prop))
  if (res && !shouldPass) res.status(412).send({ success: false, message: 'Expected to receive: ' + String(array), code: 'requiredPropsDontMatch' })
  return shouldPass
}
