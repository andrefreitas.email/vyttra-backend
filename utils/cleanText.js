function removeAcento(text) {
  text = text.toLowerCase();
  text = text.replace(new RegExp('[ÁÀÂÃÄ]', 'gi'), 'a');
  text = text.replace(new RegExp('[ÉÈÊ]', 'gi'), 'e');
  text = text.replace(new RegExp('[ÍÌÎ]', 'gi'), 'i');
  text = text.replace(new RegExp('[ÓÒÔÕ]', 'gi'), 'o');
  text = text.replace(new RegExp('[ÚÙÛ]', 'gi'), 'u');
  text = text.replace(new RegExp('[Ç]', 'gi'), 'c');
  return text;
}

const removeSpaces = text => text.replace(/\s/g, '-');

const removeSomeSymbols = text =>
  text.replace(/[\.\!\@\#\$\%\^\&\*\(\)\)\+]/g, '');

module.exports = text => {
  let finalParsedText = text.toLowerCase();
  finalParsedText = finalParsedText.trim();
  finalParsedText = removeSpaces(finalParsedText);
  finalParsedText = removeAcento(finalParsedText);
  finalParsedText = removeSomeSymbols(finalParsedText);

  return finalParsedText;
};
