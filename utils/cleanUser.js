const DEFAULT_PROPS_TO_REMOTE = [
  '_id',
  'status',
  'active',
  'createdAt',
  'updatedAt'
];
module.exports = (user, removeIt = DEFAULT_PROPS_TO_REMOTE) => {
  const copiedUser = Object.assign({}, user);
  removeIt.forEach(itemToRemove => delete copiedUser[itemToRemove]);
  return copiedUser;
};
