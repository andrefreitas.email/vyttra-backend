module.exports = {
  apps: [
    {
      name: 'API',
      script: 'main.js',

      args: 'one two',
      instances: 1,
      autorestart: true,
      watch: true,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
        DATABASE_URL: 'mongodb://127.0.0.1:27017/vyttra',
        START_HASH: 'f6e90fe24836f2a052e2d0cfc0d946c1',
        FINISH_HASH: '52bf7d55d0941e19095fc1a86776a8df',
        SENDGRID_API_KEY:
          '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',
        PORT: 5000
      },
      env_production: {
        NODE_ENV: 'production'
      }
    }
  ],

  deploy: {
    production: {
      user: 'node',
      host: '212.83.163.1',
      ref: 'origin/master',
      repo: 'git@github.com:repo.git',
      path: '/var/www/production',
      'post-deploy':
        'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
