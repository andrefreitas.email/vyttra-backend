const NODE_ENV = process.env.NODE_ENV || 'development';
const express = require('express');
const connectDb = require('./models/index');
const cors = require('cors');
const helmet = require('helmet');
const app = express();

require('custom-env').env(NODE_ENV);
const port = process.env.PORT_MAIN || 5000;
const whitelist = [
  'http://localhost:3000',
  'http://localhost:45678',
  'https://vyttra.com',
  'http://vyttra.com',
  'https://v2.vyttra.com',
  'http://v2.vyttra.com'
];
const corsOptions = {
  origin: function(origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  }
};

console.log('Start cors');
app.use(cors(corsOptions));
console.log('End cors');
console.log('Start controllers');
app.use(express.json());

[
  { path: 'produtoFinal', controllerName: 'produtoFinal', href: 'produto' },
  {
    path: 'categoriaProduct',
    controllerName: 'Product Category',
    href: 'categoriaProduct'
  },
  {
    path: 'subCategoriaProduct',
    controllerName: 'Product Sub-Category',
    href: 'subCategoriaProduct'
  },
  {
    path: 'admin',
    controllerName: 'Admin',
    href: '21232f297a57a5a743894a0e4a801fc3'
  },
  { path: 'user', controllerName: 'User', href: 'user' },
  { path: 'slider', controllerName: 'Slider', href: 'slider' },
  { path: 'blog', controllerName: 'Blog', href: 'posts' },
  { path: 'linha', controllerName: 'Linha', href: 'linha' },
  { path: 'subLinha', controllerName: 'subLinha', href: 'sublinha' },
  { path: 'marca', controllerName: 'Marca', href: 'marca' },
  { path: 'segment', controllerName: 'Segment', href: 'segment' },
  { path: 'tipo', controllerName: 'Tipo', href: 'tipo' },
  { path: 'email', controllerName: 'Email', href: 'email' },
  { path: 'contato', controllerName: 'Contato', href: 'contato' },
  { path: 'files', controllerName: 'Files', href: 'files' }
].forEach(route => {
  console.log('Start controller: ' + route.controllerName);
  app.use(
    `${((NODE_ENV === 'development' && '/') || '//') + route.href}`,
    require('./controllers/' + route.path + 'Controller')
  );
});

app.use(helmet());
console.log('END controllers');
console.log('Vyttra on air');

connectDb.mongoConnect.then(async () => {
  app.listen(port, () => console.log(`Listening on port ${port}`));
});
