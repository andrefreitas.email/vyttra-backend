const { exec } = require("child_process");
const COLOR_GREEN = "\x1b[32m";
const commands = [
  "git fetch -a",
  "git checkout release/deploy",
  "git pull origin master",
  "git push origin release/deploy",
  "git checkout -"
];

exec(commands.join(";"), (errow, result, msgError) => {
  if (errow) return console.log(errow);
  console.log(COLOR_GREEN, "Deploy completo");
});
