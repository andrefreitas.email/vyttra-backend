const fs = require('fs');

const toTitleCase = s =>
  s.substr(0, 1).toUpperCase() + s.substr(1).toLowerCase();

const modelPattern = pattern =>
  `const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ${pattern}Schema = new Schema({
  name: { type: String, required: true },
  active: { type: Boolean, default: false }
})

module.exports = mongoose.model('${pattern}', ${pattern}Schema)
`;

const controllerPattern = pattern => {
  const uppercasedPattern = toTitleCase(pattern);
  return `const ${pattern}Controller = require('express').Router()
const ${uppercasedPattern} = require('../models/${pattern}')
const validateAdmin = require('../utils/validateAdmin')

${pattern}Controller.get('/', async (req, res) => {
  const ${pattern} = await ${uppercasedPattern}.find({ active: true })
  return res.send({ success: true, ${pattern} })
})

${pattern}Controller.get('/admin', validateAdmin, async (req, res) => {
  const ${pattern} = await ${uppercasedPattern}.find()
  return res.send({ success: true, ${pattern} })
})

${pattern}Controller.post('/new', validateAdmin, async (req, res) => {
  const ${pattern} = req.body
  try {
    const new${uppercasedPattern} = await ${uppercasedPattern}.create(${pattern})
    return res.send({ success: true, ${pattern}: new${uppercasedPattern} })
  } catch (error) {
    return res.send({ success: false, message: JSON.stringify(error), code: 'ErrorCreating${uppercasedPattern}' })
  }
})

${pattern}Controller.delete('/:_id', validateAdmin, async (req, res) => {
  const { _id } = req.params;

  ${uppercasedPattern}.deleteOne({ _id })
    .then(() => res.send({ success: true }))
    .catch(() => res.send({ success: false }));
});

${pattern}Controller.put('/', validateAdmin, async (req, res) => {
  const ${pattern} = req.body
  const thisId = ${pattern} && String(${pattern}._id)

  const ${pattern}Found = await ${uppercasedPattern}.find({ _id: thisId }).limit(1)
  if (!${pattern}Found.length) return res.send({ success: false, message: '${uppercasedPattern} not found', code: '${uppercasedPattern}NotFound' })

  delete ${pattern}._id

  try {
    await ${uppercasedPattern}.updateOne({ _id: thisId }, ${pattern} )
    return res.send({ success: true, message: '${uppercasedPattern} updated sucessfully' })
  } catch (error) {
    console.log(error);
    return res.send({ success: false, message: 'Error updating ${pattern}', code: '${uppercasedPattern}NotUpdated' })
  }
})

module.exports = ${pattern}Controller
`;
};

['notification'].forEach(item => {
  fs.writeFileSync(
    `./controllers/${item}Controller.js`,
    controllerPattern(item)
  );
  fs.writeFileSync(`./models/${item}.js`, modelPattern(item));
});
