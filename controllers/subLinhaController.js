const subLinhaController = require('express').Router();
const Sublinha = require('../models/subLinha');
const validateAdmin = require('../utils/validateAdmin');

subLinhaController.get('/', async (req, res) => {
  const subLinha = await Sublinha.find({ active: true });
  return res.send({ success: true, subLinha });
});

subLinhaController.get('/admin', validateAdmin, async (req, res) => {
  const subLinha = await Sublinha.find();
  return res.send({ success: true, subLinha });
});

subLinhaController.post('/new', validateAdmin, async (req, res) => {
  const subLinha = req.body;
  try {
    const newSublinha = await Sublinha.create(subLinha);
    return res.send({ success: true, subLinha: newSublinha });
  } catch (error) {
    return res.send({
      success: false,
      message: JSON.stringify(error),
      code: 'ErrorCreatingSublinha'
    });
  }
});

subLinhaController.delete('/:_id', validateAdmin, async (req, res) => {
  const { _id } = req.params;

  Sublinha.deleteOne({ _id })
    .then(() => res.send({ success: true }))
    .catch(() => res.send({ success: false }));
});

subLinhaController.put('/', validateAdmin, async (req, res) => {
  const subLinha = req.body;
  const thisId = subLinha && String(subLinha._id);

  const subLinhaFound = await Sublinha.find({ _id: thisId }).limit(1);
  if (!subLinhaFound.length)
    return res.send({
      success: false,
      message: 'Sublinha not found',
      code: 'SublinhaNotFound'
    });

  delete subLinha._id;

  try {
    await Sublinha.updateOne({ _id: thisId }, subLinha);
    return res.send({ success: true, message: 'Sublinha updated sucessfully' });
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: 'Error updating subLinha',
      code: 'SublinhaNotUpdated'
    });
  }
});

module.exports = subLinhaController;
