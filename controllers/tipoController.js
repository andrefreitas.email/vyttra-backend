const tipoController = require('express').Router()
const Tipo = require('../models/tipo')
const validateAdmin = require('../utils/validateAdmin')

tipoController.get('/', async (req, res) => {
  const tipo = await Tipo.find({ active: true })
  return res.send({ success: true, tipo })
})

tipoController.get('/admin', validateAdmin, async (req, res) => {
  const tipo = await Tipo.find()
  return res.send({ success: true, tipo })
})

tipoController.post('/new', validateAdmin, async (req, res) => {
  const tipo = req.body
  try {
    const newTipo = await Tipo.create(tipo)
    return res.send({ success: true, tipo: newTipo })
  } catch (error) {
    return res.send({ success: false, message: JSON.stringify(error), code: 'ErrorCreatingTipo' })
  }
})

tipoController.delete('/:_id', validateAdmin, async (req, res) => {
  const { _id } = req.params;

  Tipo.deleteOne({ _id })
    .then(() => res.send({ success: true }))
    .catch(() => res.send({ success: false }));
});

tipoController.put('/', validateAdmin, async (req, res) => {
  const tipo = req.body
  const thisId = tipo && String(tipo._id)

  const tipoFound = await Tipo.find({ _id: thisId }).limit(1)
  if (!tipoFound.length) return res.send({ success: false, message: 'Tipo not found', code: 'TipoNotFound' })

  delete tipo._id

  try {
    await Tipo.updateOne({ _id: thisId }, tipo )
    return res.send({ success: true, message: 'Tipo updated sucessfully' })
  } catch (error) {
    console.log(error);
    return res.send({ success: false, message: 'Error updating tipo', code: 'TipoNotUpdated' })
  }
})

module.exports = tipoController
