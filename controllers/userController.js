const userController = require('express').Router();
const User = require('../models/user');
const encrypt = require('../utils/encrypt');
const validateIsLoggedIn = require('../utils/validateIsLoggedIn');
const validateAdmin = require('../utils/validateAdmin');
const validateUser = require('../utils/validateUser');
const validateLogin = require('../utils/validateIsLoggedIn');
const testPassword = require('../utils/validatePassword');
const haveAllProperties = require('../utils/haveAllProperties');
const cleanUser = require('../utils/cleanUser');
const POPULATE = 'notification';

userController.post('/', validateIsLoggedIn, async (req, res) => {
  const { email, password } = req.body;
  const user = await User.find({ email: email })
    .populate(POPULATE)
    .limit(1);
  if (!user.length)
    return res.status(401).send({
      success: false,
      message: 'Email not found',
      code: 'emailNotFound'
    });

  const currentUser = user[0].toObject();
  const isMatch = encrypt.compare(email + password, currentUser.login);
  if (!isMatch)
    return res.send({
      success: false,
      message: 'Email or pass are wrong',
      code: 'loginIncorrect'
    });

  return res.send({ success: true, user: currentUser });
});

userController.post('/new', async (req, res) => {
  const { email, pass: password } = req.body;

  const passedProps = haveAllProperties(
    req.body,
    [
      'cnpj',
      'razao',
      'tel',
      'cidade',
      'estado',
      'distribuidor',
      'name',
      'saudacao',
      'lastname',
      'email',
      'emailconfirmation',
      // 'registro',
      'cel',
      'pass',
      'passconfirmation',
      'termos',
      'isNewsletter'
    ],
    res
  );
  if (!passedProps) return;

  if (!testPassword(password, res)) return;

  req.body.login = await encrypt.encrypt(email + password);
  delete req.body.resetCode;
  try {
    await User.create(req.body);
    return res.send({ success: true });
  } catch (error) {
    return res.status(403).send({
      success: false,
      message: JSON.stringify(error),
      code: 'errorCreating'
    });
  }
});

userController.put('/', validateUser, async (req, res) => {
  const user = req.body;
  const userFound = await User.find({
    $or: [{ _id: user._id }, { email: user.email }]
  }).limit(1);
  if (!userFound.length)
    return res.send({
      success: false,
      message: 'User not found',
      code: 'UserNotFound'
    });
  const thisUserFound = userFound[0];
  delete user.cnpj;
  delete user.razao;
  delete user.resetCode;
  try {
    await User.updateOne({ _id: thisUserFound._id }, user);
    return res.send({ success: true, message: 'User updated sucessfully' });
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: 'Error updating user',
      code: 'UserNotUpdated'
    });
  }
});

userController.post('/letsgo', validateLogin, async (req, res) => {
  const { email, password } = req.body;

  const user = await User.find({ email, active: true }).limit(1);
  if (!user.length)
    return res.status(401).send({
      success: false,
      message: 'Email not found',
      code: 'emailNotFound'
    });

  const currentUser = user[0].toObject();

  const isMatch = encrypt.compare(email + password, currentUser.login);
  if (!isMatch)
    return res.status(401).send({
      success: false,
      message: 'Email or pass are wrong',
      code: 'loginIncorrect'
    });
  const cleanedUser = cleanUser(currentUser);
  return res.send({ success: true, user: cleanedUser });
});

userController.get('/all/:page', validateAdmin, async (req, res) => {
  const { page = 1 } = req.params;
  const users = await User.paginate(
    {},
    {
      page,
      sort: { createdAt: -1 },
      limit: 20
    }
  );

  return res.send({ success: true, users });
});

userController.post('/some/', validateAdmin, async (req, res) => {
  const { query } = req.body;
  if (!query)
    res.send({
      success: false,
      message: 'Expected to receive name or email',
      code: 'NameOrEmailEmpty'
    });

  const couldMatchWith = [
    { name: new RegExp(query) },
    { email: new RegExp(query) }
  ];
  const usersFound = await User.find({ $or: couldMatchWith });
  if (!usersFound.length)
    res.send({
      success: false,
      message: 'User not found',
      code: 'UserNotFound'
    });

  return res.send({ success: true, users: usersFound });
});

userController.post('/reset/', async (req, res) => {
  const { resetCode, password } = req.body;
  const usersFound = await User.findOne({ resetCode });
  if (!usersFound)
    res.send({
      success: false,
      message: 'User not found',
      code: 'UserNotFound'
    });

  try {
    const login = await encrypt.encrypt(usersFound.email + password);
    await User.findByIdAndUpdate(
      { _id: usersFound._id },
      { login, resetCode: '' }
    );
    return res.send({ success: true });
  } catch (error) {
    res.send({
      success: false,
      message: 'Não foi possivel atualizar a senha'
    });
  }
});

module.exports = userController;
