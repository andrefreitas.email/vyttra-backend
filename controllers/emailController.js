const sgMail = require('@sendgrid/mail');
const emailController = require('express').Router();
const Email = require('../models/email');
const User = require('../models/user');
const sendEmail = require('../utils/sendEmail');
const validateEmail = require('../utils/validateEmail');
const validateAdmin = require('../utils/validateAdmin');
const currentUrlSite = 'http://localhost/area-logada/senha';
require('custom-env').env(process.env.NODE_ENV);
const DEFAULT_FROM = 'contato@vyttra.com';

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

emailController.post('/newsletter', (req, res) => {
  const { email } = req.body;
  try {
    if (!validateEmail(email)) throw new Error('Email incorreto!');
    Email.create({ email })
      .then(() => {
        sendEmail({
          to: email,
          subject: 'Bem vindo a nossa newsletter',
          text: 'Bem vindo a nossa newsletter',
        });
        sgMail.send({
          to: DEFAULT_FROM,
          from: DEFAULT_FROM,
          subject: 'Novo contato de newsletter',
          text: `O contato ${email} acaba de se cadastrar em nossa newsletter`,
        });
        return res.send({
          success: true,
          message: 'Email enviado com sucesso',
          codeMessage: 'successMail',
        });
      })
      .catch(error => {
        throw new Error(error);
      });
  } catch (error) {
    res.send({
      success: false,
      message: 'Erro ao cadastrar a newsletter',
      codeMessage: error,
    });
  }
});

emailController.get('/newsletter/export', (req, res) => {
  try {
    Email.find({admin:false})
      .then((response) =>{
        res.send(response)
      })
      .catch(error => {
        throw new Error(error);
      });
  } catch (error) {
    res.send({
      success: false,
      message: 'Erro ao cadastrar a newsletter',
      codeMessage: error,
    });
  }
});


emailController.post('/form', async (req, res) => {
  const { content } = req.body;

  const type = content.name[0];
  const toSaved = await Email.findOne({ type, admin: true });

  const to = toSaved.email.split(';').map(email => email.trim());

  // const toTarget = {
  //   t: 'rhdp@vyttra.com',
  //   d: 'digitalcare@vyttra.com',
  //   c: 'karina.siqueira@vyttra.com',
  //   p: 'marketing@vyttra.com',
  //   f: 'contasareceber@vyttra.com',
  // };

  const cc = ['marketing@vyttra.com'];
  const {
    nome,
    telefone,
    cnpj,
    razao,
    cep,
    cidade,
    estado,
    assunto,
    mensagem,
    celular,
    ajudar,
    linha,
    regiao,
    cv
  } = content.answer;
  email = content.answer['e-mail'];
  const text = `Olá,
  Um contato enviou uma mensagem através do formulário ${
    content.name
    }, os dados são:
  ${(nome && 'nome: ' + nome + ',') || ''}
  ${(email && 'email: ' + email + ',') || ''}
  ${(telefone && 'telefone: ' + telefone + ',') || ''}
  ${(celular && 'celular: ' + celular + ',') || ''}
  ${(cnpj && 'cnpj: ' + cnpj + ',') || ''}
  ${(razao && 'razao: ' + razao + ',') || ''}
  ${(cep && 'cep: ' + cep + ',') || ''}
  ${(cidade && 'cidade: ' + cidade + ',') || ''}
  ${(estado && 'estado: ' + estado + ',') || ''}
  ${(ajudar && 'interesse: ' + ajudar + ',') || ''}
  ${(linha && 'linha: ' + linha + ',') || ''}
  ${(regiao && 'regiao: ' + regiao + ',') || ''}
  ${(assunto && 'assunto: ' + assunto + ',') || ''}
  ${(mensagem && 'mensagem: ' + mensagem) || ''}
  ${(cv && 'Link do cv: ' + cv) || ''}
  `;
  sgMail
    .send({
      to,
      cc,
      from: DEFAULT_FROM,
      subject: 'Novo contato do formulário ' + content.name,
      text,
    })
    .then(() => {
      console.log('Enviado!');
      return res.send({
        success: true,
        message: 'Email enviado com sucesso',
        codeMessage: 'successMail',
      });
    })
    .catch(error => {
      console.log('Erro ao enviar email ' + content);
      res.send({
        success: false,
        message: 'Erro ao cadastrar',
        codeMessage: error,
      });
    });
});

emailController.post('/invite', (req, res) => {
  const { email, name, inviter } = req.body;
  if (!validateEmail(email)) {
    return res.send({
      success: false,
      message: 'Email incorreto!',
    });
  }
  const msg = {
    to: email,
    from: DEFAULT_FROM,
    templateId: 'd-0a40e2812ce64b1daedeff4d70d95898',
    dynamic_template_data: {
      subject: 'Convite especial Vyttra',
      name,
      inviter,
    },
  };
  sgMail
    .send(msg)
    .then(() => {
      return res.send({
        success: true,
        message: 'Email enviado com sucesso',
        codeMessage: 'successMail',
      });
    })
    .catch(error => {
      res.send({
        success: false,
        message: 'Erro ao cadastrar',
        codeMessage: error,
      });
    });
});

emailController.post('/welcome', (req, res) => {
  const { email, name } = req.body;
  if (!validateEmail(email)) {
    return res.send({
      success: false,
      message: 'Email incorreto!',
    });
  }
  const msg = {
    to: email,
    from: DEFAULT_FROM,
    subject: 'Bem-vindo à Vyttra',
    templateId: 'd-046d75eb45614047ba531516d3f1b398',
    dynamic_template_data: {
      subject: 'Bem-vindo à Vyttra',
      name
    },
  };
  sgMail
    .send(msg)
    .then(() => {
      return res.send({
        success: true,
        message: 'Email enviado com sucesso',
        codeMessage: 'successMail',
      });
    })
    .catch(error => {
      res.send({
        success: false,
        message: 'Erro ao cadastrar',
        codeMessage: error,
      });
    });
});

emailController.post('/reset', async (req, res) => {
  const { email } = req.body;
  if (!validateEmail(email)) {
    return res.send({
      success: false,
      message: 'Email incorreto!',
    });
  }

  const resetCode = Math.random()
    .toString(36)
    .substring(7);

  const currentUser = await User.findOne({ email });
  if (!currentUser) {
    return res.send({
      success: false,
      message: 'Email não encontrado!',
      codeMessage: error,
    });
  }

  User.findByIdAndUpdate(currentUser._id, { resetCode })
    .then(() => {
      const resetUrl = `${currentUrlSite}/${resetCode}`;
      const msg = {
        to: email,
        from: DEFAULT_FROM,
        templateId: 'd-e4a1ea81f8ad4a69966b5b19686dfdc7',
        dynamic_template_data: {
          subject: 'Recuperação de Senha Vyttra',
          resetUrl,
        },
      };
      sgMail
        .send(msg)
        .then(() => {
          return res.send({
            success: true,
            message: 'Email enviado com sucesso',
            codeMessage: 'successMail',
          });
        })
        .catch(bad => {
          throw new Error(bad);
        });
    })
    .catch(error => {
      res.send({
        success: false,
        message: 'Erro ao recuperar',
        codeMessage: error,
      });
    });
});

emailController.get('/admin', validateAdmin, async (req, res) => {
  return Email.find({ admin: true })
    .then(emailsOfAdmin => {
      return res.send({
        success: true,
        emailsOfAdmin,
      });
    })
    .catch(error =>
      res.send({
        success: false,
        message: JSON.stringify(error),
      })
    );
});

emailController.post('/admin', validateAdmin, async (req, res) => {
  const { type } = req.body;

  await Email.deleteOne({ admin: true, type });

  return Email.create({ ...req.body, admin: true })
    .then(emailsOfAdmin => {
      return res.send({
        success: true,
        message: 'Emails admin updated successfully',
        emailsOfAdmin,
      });
    })
    .catch(error =>
      res.send({
        success: false,
        message: 'Error adding Emails' + JSON.stringify(error),
      })
    );
});

module.exports = emailController;
