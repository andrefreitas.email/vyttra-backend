const categoriaProductController = require('express').Router();
const Categoriaproduct = require('../models/categoriaProduct');
const validateAdmin = require('../utils/validateAdmin');

categoriaProductController.get('/', async (req, res) => {
  const categoriaProduct = await Categoriaproduct.find({ active: true });
  return res.send({ success: true, categoriaProduct });
});

categoriaProductController.get('/admin', validateAdmin, async (req, res) => {
  const categoriaProduct = await Categoriaproduct.find();
  return res.send({ success: true, categoriaProduct });
});

categoriaProductController.post('/new', validateAdmin, async (req, res) => {
  const categoriaProduct = req.body;
  try {
    const newCategoriaproduct = await Categoriaproduct.create(categoriaProduct);
    return res.send({ success: true, categoriaProduct: newCategoriaproduct });
  } catch (error) {
    return res.send({
      success: false,
      message: JSON.stringify(error),
      code: 'ErrorCreatingCategoriaproduct'
    });
  }
});

categoriaProductController.delete('/:_id', validateAdmin, async (req, res) => {
  const { _id } = req.params;

  Categoriaproduct.deleteOne({ _id })
    .then(() => res.send({ success: true }))
    .catch(() => res.send({ success: false }));
});

categoriaProductController.put('/', validateAdmin, async (req, res) => {
  const categoriaProduct = req.body;
  const thisId = categoriaProduct && String(categoriaProduct._id);

  const categoriaProductFound = await Categoriaproduct.find({
    _id: thisId
  }).limit(1);
  if (!categoriaProductFound.length)
    return res.send({
      success: false,
      message: 'Categoriaproduct not found',
      code: 'CategoriaproductNotFound'
    });

  delete categoriaProduct._id;

  try {
    await Categoriaproduct.updateOne({ _id: thisId }, categoriaProduct);
    return res.send({
      success: true,
      message: 'Categoriaproduct updated sucessfully'
    });
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: 'Error updating categoriaProduct',
      code: 'CategoriaproductNotUpdated'
    });
  }
});

module.exports = categoriaProductController;
