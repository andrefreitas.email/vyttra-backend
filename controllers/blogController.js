const blogController = require('express').Router();
const Post = require('../models/post');
const BlogConfig = require('../models/blogConfig');
const CategoryPost = require('../models/categoryPost');
const validateAdmin = require('../utils/validateAdmin');
const cleanText = require('../utils/cleanText');
const POPULATE_POST = 'author category';

blogController.get('/config', async (req, res) => {
  const config = await BlogConfig.findOne({});
  return res.send({ success: true, config });
});

blogController.post('/config', validateAdmin, async (req, res) => {
  console.log(req.body);
  await BlogConfig.updateOne({}, req.body, { upsert: 1 })
    .then(data => res.send({ success: true, data }))
    .catch(error => res.send({ success: false, error }))
});

blogController.get('/categories', async (req, res) => {
  const categories = await CategoryPost.find({ active: true });
  return res.send({ success: true, categories });
});

blogController.get('/categories/admin', validateAdmin, async (req, res) => {
  const categories = await CategoryPost.find();
  return res.send({ success: true, categories });
});

blogController.post('/categories/new', validateAdmin, async (req, res) => {
  const category = req.body;
  try {
    const newCategoryPost = await CategoryPost.create(category);
    return res.send({ success: true, category: newCategoryPost });
  } catch (error) {
    return res.send({
      success: false,
      message: JSON.stringify(error),
      code: 'ErrorCreatingCategory'
    });
  }
});

blogController.put('/categories/', validateAdmin, async (req, res) => {
  const category = req.body;
  const thisId = category && String(category._id);

  const categoryFound = await CategoryPost.find({ _id: thisId }).limit(1);
  if (!categoryFound.length)
    return res.send({
      success: false,
      message: 'CategoryPost not found',
      code: 'CategoryPostNotFound'
    });

  delete category._id;

  try {
    await CategoryPost.updateOne({ _id: thisId }, category);
    return res.send({
      success: true,
      message: 'CategoryPost updated sucessfully'
    });
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: 'Error updating category',
      code: 'CategoryPostNotUpdated'
    });
  }
});

// posts

blogController.get('/', async (req, res) => {
  const posts = await Post.find({ active: true })
    .populate(POPULATE_POST, 'name')
    .sort({ _id: -1 });
  return res.send({ success: true, posts });
});

blogController.get('/lasts', async (req, res) => {
  const posts = await Post.find({ active: true })
    .sort({ _id: -1 })
    .limit(4);
  return res.send({ success: true, posts });
});

blogController.get('/:name', async (req, res) => {
  const { name } = req.params;
  const post = await Post.findOne({ url: name, active: true }).populate(
    POPULATE_POST,
    'name'
  );
  if (!post)
    return res.send({
      success: false,
      message: 'Post não encontrado :(',
      code: 'ErrorCreatingPost'
    });
  return res.send({ success: true, post });
});

blogController.post('/admin', validateAdmin, async (req, res) => {
  const posts = await Post.find()
    .populate(POPULATE_POST, 'name')
    .sort({ _id: -1 });
  return res.send({ success: true, posts });
});

blogController.delete('/:_id', validateAdmin, async (req, res) => {
  const { _id } = req.params;

  Post.deleteOne({ _id })
    .then(() => res.send({ success: true }))
    .catch(() => res.send({ success: false }));
});

blogController.post('/new', validateAdmin, async (req, res) => {
  const post = req.body;
  const randomNumber = Math.max(Math.random() * 10 * 10).toFixed(10);
  post.url = cleanText(post.name || randomNumber);
  try {
    const newPost = await Post.create(post);
    const savedPost = await Post.findById(newPost._id).populate(
      'author',
      'name'
    );
    return res.send({ success: true, post: savedPost });
  } catch (error) {
    return res.send({
      success: false,
      message: JSON.stringify(error),
      code: 'ErrorCreatingPost'
    });
  }
});

blogController.put('/', validateAdmin, async (req, res) => {
  const post = req.body;
  const thisId = post && String(post._id);

  const postFound = await Post.find({ _id: thisId }).limit(1);
  if (!postFound.length)
    return res.send({
      success: false,
      message: 'Post not found',
      code: 'PostNotFound'
    });

  delete post._id;

  try {
    await Post.updateOne({ _id: thisId }, post);
    return res.send({ success: true, message: 'Post updated sucessfully' });
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: 'Error updating post',
      code: 'PostNotUpdated'
    });
  }
});

module.exports = blogController;
