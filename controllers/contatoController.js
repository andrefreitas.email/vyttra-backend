const contatoController = require('express').Router();
const Contato = require('../models/contato');
const validateAdmin = require('../utils/validateAdmin');

contatoController.get('/admin', validateAdmin, async (req, res) => {
  const contato = await Contato.find();
  return res.send({ success: true, contato });
});

contatoController.post('/new', async (req, res) => {
  const contato = req.body;
  try {
    const newContato = await Contato.create(contato);
    return res.send({ success: true, contato: newContato });
  } catch (error) {
    return res.send({
      success: false,
      message: JSON.stringify(error),
      code: 'ErrorCreatingContato'
    });
  }
});

contatoController.delete('/:_id', validateAdmin, async (req, res) => {
  const { _id } = req.params;

  Contato.deleteOne({ _id })
    .then(() => res.send({ success: true }))
    .catch(() => res.send({ success: false }));
});

contatoController.put('/', validateAdmin, async (req, res) => {
  const contato = req.body;
  const thisId = contato && String(contato._id);

  const contatoFound = await Contato.find({ _id: thisId }).limit(1);
  if (!contatoFound.length)
    return res.send({
      success: false,
      message: 'Contato not found',
      code: 'ContatoNotFound'
    });

  delete contato._id;

  try {
    await Contato.updateOne({ _id: thisId }, contato);
    return res.send({ success: true, message: 'Contato updated sucessfully' });
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: 'Error updating contato',
      code: 'ContatoNotUpdated'
    });
  }
});

module.exports = contatoController;
