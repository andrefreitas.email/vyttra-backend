const segmentController = require('express').Router();
const Segment = require('../models/segment');
const validateAdmin = require('../utils/validateAdmin');
const cleanText = require('../utils/cleanText');

segmentController.get('/admin', validateAdmin, async (req, res) => {
  const segment = await Segment.find();
  return res.send({ success: true, segment });
});

segmentController.get('/:name', async (req, res) => {
  const { name } = req.params;
  const segment = await Segment.find({
    active: true
  });
  const result = segment.find(item => cleanText(item.name) === name);
  return res.send({ success: true, _id: result._id });
});

segmentController.get('/', async (req, res) => {
  const segment = await Segment.find({ active: true });
  return res.send({ success: true, segment });
});

segmentController.delete('/:_id', validateAdmin, async (req, res) => {
  const { _id } = req.params;

  Segment.deleteOne({ _id })
    .then(() => res.send({ success: true }))
    .catch(() => res.send({ success: false }));
});

segmentController.post('/new', validateAdmin, async (req, res) => {
  const segment = req.body;

  Segment.create(segment)
    .then(newSegment => {
      return res.send({ success: true, segment: newSegment });
    })
    .catch(error => {
      return res.send({
        success: false,
        message: JSON.stringify(error),
        code: 'ErrorCreatingSegment'
      });
    });
});

segmentController.put('/', validateAdmin, async (req, res) => {
  const segment = req.body;
  const thisId = segment && String(segment._id);

  const segmentFound = await Segment.find({ _id: thisId }).limit(1);
  if (!segmentFound.length)
    return res.send({
      success: false,
      message: 'Segment not found',
      code: 'SegmentNotFound'
    });

  delete segment._id;

  try {
    await Segment.updateOne({ _id: thisId }, segment);
    return res.send({ success: true, message: 'Segment updated sucessfully' });
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: 'Error updating segment',
      code: 'SegmentNotUpdated'
    });
  }
});

module.exports = segmentController;
