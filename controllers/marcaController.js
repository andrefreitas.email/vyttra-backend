const marcaController = require('express').Router();
const Marca = require('../models/marca');
const validateAdmin = require('../utils/validateAdmin');

marcaController.get('/', async (req, res) => {
  const marca = await Marca.find({ active: true });
  return res.send({ success: true, marca });
});

marcaController.get('/admin', validateAdmin, async (req, res) => {
  const marca = await Marca.find();
  return res.send({ success: true, marca });
});

marcaController.post('/new', validateAdmin, async (req, res) => {
  const marca = req.body;
  try {
    const newMarca = await Marca.create(marca);
    return res.send({ success: true, marca: newMarca });
  } catch (error) {
    return res.send({
      success: false,
      message: JSON.stringify(error),
      code: 'ErrorCreatingMarca'
    });
  }
});

marcaController.delete('/:_id', validateAdmin, async (req, res) => {
  const { _id } = req.params;

  Marca.deleteOne({ _id })
    .then(() => res.send({ success: true }))
    .catch(() => res.send({ success: false }));
});

marcaController.put('/', validateAdmin, async (req, res) => {
  const marca = req.body;
  const thisId = marca && String(marca._id);

  const marcaFound = await Marca.find({ _id: thisId }).limit(1);
  if (!marcaFound.length)
    return res.send({
      success: false,
      message: 'Marca not found',
      code: 'MarcaNotFound'
    });

  delete marca._id;

  try {
    await Marca.updateOne({ _id: thisId }, marca);
    return res.send({ success: true, message: 'Marca updated sucessfully' });
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: 'Error updating marca',
      code: 'MarcaNotUpdated'
    });
  }
});

module.exports = marcaController;
