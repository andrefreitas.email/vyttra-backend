const sliderController = require('express').Router()
const Slider = require('../models/slider')
const haveAllProperties = require('../utils/haveAllProperties')
const validateIsLoggedIn = require('../utils/validateIsLoggedIn')
const validateAdmin = require('../utils/validateAdmin')

sliderController.get('/', async (req, res) => {
  const allSliders = await Slider.find().limit(30)
  return res.send({ success: true, slider: allSliders })
})

sliderController.get('/page/:page', async (req, res) => {
  const { page } = req.params;
  const pagesPaginated = await Slider.find({ page }).limit(3)
  return res.send({ success: true, slider: pagesPaginated })
})

sliderController.post('/', validateIsLoggedIn, async (req, res) => {
  const receivedSlider = req.body;
  const validatePage = haveAllProperties(page)

  if (!validatePage) return res.status(412).send({ success: false, message: 'Expected to receive [name, banner, page]', code: 'requiredPropsDontMatch' })

  const currentSlider = await Slider.findOne({ _id: receivedSlider._id }, (error) => {
    if (error) return res.status(404).send({ success: false, message: 'Slider not found' + String(error), code: 'sliderNotFound' })
  })
  const foundSlider = currentSlider.toObject()

  Object.assign(foundSlider, receivedSlider)
  await Slider.updateOne({_id: receivedSlider._id}, foundSlider)

  return res.send({ success: true, slider: receivedSlider })
})

sliderController.post('/', validateAdmin, async (req, res) => {
  const login = await encrypt.encrypt(email + password)
  try {
    const adminUser = await Admin.create({
      name,
      email,
      login
    })
    return res.send({ success: true, adminUser })
  } catch (error) {
    return res.send({ success: false, message: JSON.stringify(error), code: 'errorCreating' })
  }
})

sliderController.put('/', validateAdmin, async (req, res) => {
  const { email, password, newPassword } = req.body
  const adminFound = await Admin.find({ email: email }).limit(1)
  if (!adminFound) return res.send({ success: false, message: 'Email not found', code: 'EmailNotFound' })

  const isLogin = await encrypt.compare(email + password, adminFound.login)
  if (!isLogin) return res.send({ success: false, message: 'Email or password doesnt match', code: 'emailOrPassWrong'})

  try {
    await Admin.updateOne({ email: email }, { password: newPassword })
    return res.send({ success: true, message: 'Password updated successfully' })
  } catch (error) {
    return res.send({ success: false, message: 'Error updating password', code: 'passwordNotUpdated' })
  }
})

module.exports = sliderController
