const filesController = require('express').Router();
const Files = require('../models/files');
const validateAdmin = require('../utils/validateAdmin');

filesController.get('/', async (req, res) => {
  const files = await Files.find({
    active: true,
    type: { $not: { $eq: 'formulario' } }
  });
  return res.send({ success: true, files });
});

filesController.post('/admin', validateAdmin, async (req, res) => {
  const query = req.body.query;
  const files = await Files.find({ active: true, ...query }).sort({ _id: -1 });
  return res.send({ success: true, files });
});

filesController.post('/new', validateAdmin, async (req, res) => {
  const files = req.body;
  try {
    const newFiles = await Files.create(files);
    return res.send({ success: true, files: newFiles });
  } catch (error) {
    return res.send({
      success: false,
      message: JSON.stringify(error),
      code: 'ErrorCreatingFiles'
    });
  }
});

filesController.post('/form', async (req, res) => {
  const files = req.body;
  try {
    await Files.create(files);
    return res.send({ success: true });
  } catch (error) {
    return res.send({
      success: false,
      message: JSON.stringify(error),
      code: 'ErrorCreatingFiles'
    });
  }
});

filesController.put('/', validateAdmin, async (req, res) => {
  const files = req.body;
  const thisId = files && String(files._id);

  const filesFound = await Files.find({ _id: thisId }).limit(1);
  if (!filesFound.length)
    return res.send({
      success: false,
      message: 'Files not found',
      code: 'FilesNotFound'
    });

  delete files._id;

  try {
    await Files.updateOne({ _id: thisId }, files);
    return res.send({ success: true, message: 'Files updated sucessfully' });
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: 'Error updating files',
      code: 'FilesNotUpdated'
    });
  }
});

module.exports = filesController;
