const notificationController = require('express').Router()
const Notification = require('../models/notification')
const validateAdmin = require('../utils/validateAdmin')

notificationController.get('/', async (req, res) => {
  const notification = await Notification.find({ active: true })
  return res.send({ success: true, notification })
})

notificationController.get('/admin', validateAdmin, async (req, res) => {
  const notification = await Notification.find()
  return res.send({ success: true, notification })
})

notificationController.post('/new', validateAdmin, async (req, res) => {
  const notification = req.body
  try {
    const newNotification = await Notification.create(notification)
    return res.send({ success: true, notification: newNotification })
  } catch (error) {
    return res.send({ success: false, message: JSON.stringify(error), code: 'ErrorCreatingNotification' })
  }
})

notificationController.delete('/:_id', validateAdmin, async (req, res) => {
  const { _id } = req.params;

  Notification.deleteOne({ _id })
    .then(() => res.send({ success: true }))
    .catch(() => res.send({ success: false }));
});

notificationController.put('/', validateAdmin, async (req, res) => {
  const notification = req.body
  const thisId = notification && String(notification._id)

  const notificationFound = await Notification.find({ _id: thisId }).limit(1)
  if (!notificationFound.length) return res.send({ success: false, message: 'Notification not found', code: 'NotificationNotFound' })

  delete notification._id

  try {
    await Notification.updateOne({ _id: thisId }, notification )
    return res.send({ success: true, message: 'Notification updated sucessfully' })
  } catch (error) {
    console.log(error);
    return res.send({ success: false, message: 'Error updating notification', code: 'NotificationNotUpdated' })
  }
})

module.exports = notificationController
