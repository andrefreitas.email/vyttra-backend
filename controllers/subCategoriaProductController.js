const subCategoriaProductController = require('express').Router();
const Subcategoriaproduct = require('../models/subCategoriaProduct');
const validateAdmin = require('../utils/validateAdmin');

subCategoriaProductController.get('/', async (req, res) => {
  const subCategoriaProduct = await Subcategoriaproduct.find({ active: true });
  return res.send({ success: true, subCategoriaProduct });
});

subCategoriaProductController.get('/admin', validateAdmin, async (req, res) => {
  const subCategoriaProduct = await Subcategoriaproduct.find();
  return res.send({ success: true, subCategoriaProduct });
});

subCategoriaProductController.post('/new', validateAdmin, async (req, res) => {
  const subCategoriaProduct = req.body;
  try {
    const newSubcategoriaproduct = await Subcategoriaproduct.create(
      subCategoriaProduct
    );
    return res.send({
      success: true,
      subCategoriaProduct: newSubcategoriaproduct
    });
  } catch (error) {
    return res.send({
      success: false,
      message: JSON.stringify(error),
      code: 'ErrorCreatingSubcategoriaproduct'
    });
  }
});

subCategoriaProductController.delete(
  '/:_id',
  validateAdmin,
  async (req, res) => {
    const { _id } = req.params;

    Subcategoriaproduct.deleteOne({ _id })
      .then(() => res.send({ success: true }))
      .catch(() => res.send({ success: false }));
  }
);

subCategoriaProductController.put('/', validateAdmin, async (req, res) => {
  const subCategoriaProduct = req.body;
  const thisId = subCategoriaProduct && String(subCategoriaProduct._id);

  const subCategoriaProductFound = await Subcategoriaproduct.find({
    _id: thisId
  }).limit(1);
  if (!subCategoriaProductFound.length)
    return res.send({
      success: false,
      message: 'Subcategoriaproduct not found',
      code: 'SubcategoriaproductNotFound'
    });

  delete subCategoriaProduct._id;

  try {
    await Subcategoriaproduct.updateOne({ _id: thisId }, subCategoriaProduct);
    return res.send({
      success: true,
      message: 'Subcategoriaproduct updated sucessfully'
    });
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: 'Error updating subCategoriaProduct',
      code: 'SubcategoriaproductNotUpdated'
    });
  }
});

module.exports = subCategoriaProductController;
