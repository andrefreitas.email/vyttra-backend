const contentController = require('express').Router()
const Content = require('../models/content')
const validateAdmin = require('../utils/validateAdmin')
const haveAllProperties = require('../utils/haveAllProperties')

contentController.get('/', async (req, res) => {
  const allProducts = await Content.find()
  res.send(allProducts)
})

contentController.post('/', validateAdmin, async (req, res) => {
  const baseProperties = ['name', 'img', 'category', 'especifications']
  const product = req.body
  if (!product) return res.send({ success: false, message: 'Expected to receive a product data' })
  if (typeof product !== 'object') return res.send({ success: false, message: 'Expected to receive a object' })

  const passedByPropTest = haveAllProperties(product, baseProperties)
  if (!passedByPropTest) return res.send({ success: false, message: 'Should receive props:' + JSON.stringify(baseProperties), code: 'missingProperties'})

  const { name, img, category, especifications, pdf } = product
  try {
    await Content.create({
      name,
      img,
      category,
      especifications,
      pdf
    })
    return res.send({ success: true, message: 'Product added successfully' })
  } catch (error) {
    return res.send({ success: false, message: 'Error adding product' + JSON.stringify(error) })
  }
})

contentController.put('/', validateAdmin, async (req, res) => {
  const product = req.body
  const foundProduct = await Content.find({_id: product._id})
  if (!foundProduct.length) return res.send({ success: false, message: 'Error adding product', code: 'productNotFound' })

  const currentProduct = foundProduct[0].toObject()
  Object.assign(currentProduct , product)
  try {
    await Content.updateOne({ _id: product._id }, foundProduct)
    return res.send({ success: true, message: 'Product added successfully' })
  } catch (error) {
    return res.send({ success: false, message: 'Error adding product' + JSON.stringify(error) })
  }
})

module.exports = contentController
