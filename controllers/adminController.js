const adminController = require('express').Router()
const Admin = require('../models/admin')
const validateAdmin = require('../utils/validateAdmin')
const validateLogin = require('../utils/validateIsLoggedIn')
const testPassword = require('../utils/validatePassword')
const encrypt = require('../utils/encrypt')

adminController.post('/', validateLogin, async (req, res) => {
  const { email, password } = req.body
  const user = await Admin.find({ email, active: true }).limit(1)
  if (!user.length) return res.status(401).send({ success: false, message: 'Email not found', code: 'emailNotFound' })

  const currentUser = user[0].toObject()
  const isMatch = encrypt.compare(email + password, currentUser.login)
  if (!isMatch) return res.status(401).send({ success: false, message: 'Email or pass are wrong', code: 'loginIncorrect' })

  return res.send({ success: true, user: currentUser })
})

adminController.post('/new', validateAdmin, async (req, res) => {
  const { name, email, password } = req.body
  if (!testPassword(password, res)) return
  

  const login = await encrypt.encrypt(email + password)
  try {
    const adminUser = await Admin.create({
      name,
      email,
      login
    })
    return res.send({ success: true, adminUser })
  } catch (error) {
    return res.send({ success: false, message: JSON.stringify(error), code: 'errorCreating' })
  }
})

adminController.put('/', validateAdmin, async (req, res) => {
  const { email, password, newPassword } = req.body
  const adminFound = await Admin.find({ email: email }).limit(1)
  if (!adminFound) return res.status(401).send({ success: false, message: 'Email not found', code: 'EmailNotFound' })

  const isLogin = await encrypt.compare(email + password, adminFound.login)
  if (!isLogin) return res.status(401).send({ success: false, message: 'Email or password doesnt match', code: 'emailOrPassWrong' })

  try {
    await Admin.updateOne({ email: email }, { password: newPassword })
    return res.send({ success: true, message: 'Password updated successfully' })
  } catch (error) {
    return res.send({ success: false, message: 'Error updating password', code: 'passwordNotUpdated' })
  }
})

module.exports = adminController
