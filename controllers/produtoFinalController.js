const ProdutoFinalController = require('express').Router();
const Produto = require('../models/produtoFinal');
const File = require('../models/files');
const validateAdmin = require('../utils/validateAdmin');
const mongoose = require('mongoose');
const cleanText = require('../utils/cleanText');
const POPULATE = 'linha';

ProdutoFinalController.get('/', async (req, res) => {
  const allProdutos = await Produto.find({ active: true })
    .sort({ _id: -1 })
    .populate(POPULATE, 'name color');
  res.send(allProdutos);
});

// create and update here
ProdutoFinalController.post('/', validateAdmin, async (req, res) => {
  const produto = req.body;
  if (!produto)
    return res.send({
      success: false,
      message: 'Expected to receive a produto data'
    });
  if (typeof produto !== 'object')
    return res.send({
      success: false,
      message: 'Expected to receive a object'
    });
  const currentId = produto._id || new mongoose.Types.ObjectId();

  delete produto._id;

  produto.url = produto.url || cleanText(produto.name);

  return Produto.updateOne({ _id: currentId }, produto, {
    upsert: true,
    setDefaultsOnInsert: true
  })
    .then(produto => {
      return res.send({
        success: true,
        message: 'Produto added successfully',
        produto
      });
    })
    .catch(error =>
      res.send({
        success: false,
        message: 'Error adding produto' + JSON.stringify(error)
      })
    );
});

ProdutoFinalController.get('/admin', validateAdmin, async (req, res) => {
  const products = await Produto.find()
    .sort({ _id: -1 })
    .populate(POPULATE, 'name color');
  return res.send({ success: true, products });
});

ProdutoFinalController.delete('/:_id', validateAdmin, async (req, res) => {
  const { _id } = req.params;

  Produto.deleteOne({ _id })
    .then(() => res.send({ success: true }))
    .catch(() => res.send({ success: false }));
});

ProdutoFinalController.get('/featured', async (req, res) => {
  const products = await Produto.find({ active: true, isFeatured: true })
    .populate(POPULATE, 'name color')
    .sort({
      _id: -1
    });
  return res.send({ success: true, products });
});

ProdutoFinalController.get('/:name', async (req, res) => {
  const { name } = req.params;
  const product = await Produto.findOne({ url: name, active: true }).populate(
    POPULATE,
    'name color'
  );
  if (!product)
    return res.send({
      success: false,
      message: 'Produto não encontrado :(',
      code: 'ErrorCreatingPost'
    });
  return res.send({ success: true, product });
});

ProdutoFinalController.get('/id/:url', async (req, res) => {
  const { url } = req.params;
  const product = await Produto.findOne({ url }).populate(POPULATE)
  const file = await File.findOne({ url }).populate(POPULATE)

  const shouldReturnIt = product || file

  if (!shouldReturnIt)
    return res.send({
      success: false,
      message: 'Produto não encontrado :(',
      code: 'ErrorCreatingPost'
    });

  const result = await shouldReturnIt
  return res.send({ success: true, product: result });

});

module.exports = ProdutoFinalController;
