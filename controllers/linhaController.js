const linhaController = require('express').Router();
const Linha = require('../models/linha');
const validateAdmin = require('../utils/validateAdmin');
const updateProductsWithSegment = require('../utils/updateProductsWithSegment');
const cleanText = require('../utils/cleanText');
const OPTIONS_TO_POPULATE =
  'author categoria subCategoria subLinha marca produtoFinal segment tipo amaisFinal';

linhaController.get('/', async (req, res) => {
  const linha = await Linha.find({ active: true }).populate(
    OPTIONS_TO_POPULATE
  );
  return res.send({ success: true, linha });
});

linhaController.get('/admin', validateAdmin, async (req, res) => {
  const linha = await Linha.find().populate(OPTIONS_TO_POPULATE);
  return res.send({ success: true, linha });
});

linhaController.post('/admin-item', validateAdmin, async (req, res) => {
  const { query } = req.headers;
  const linha = await Linha.find({ name: query });
  return res.send({ success: true, linha });
});

linhaController.get('/item/:itemName', async (req, res) => {
  const { itemName } = req.params;
  const linhas = await Linha.find({ active: true, path: itemName }).populate(
    OPTIONS_TO_POPULATE
  );
  return res.send({ success: true, linhas });
});

linhaController.get('/item/menu/:itemName', async (req, res) => {
  const { itemName } = req.params;
  const linhas = await Linha.find({ active: true, path: itemName }).populate(
    'tipo'
  );
  setTimeout(() => {
    return res.send({ success: true, linhas });
  }, 1000);
});

linhaController.get('/products/:urlId', async (req, res) => {
  const { urlId } = req.params;
  const linha = await Linha.findOne({
    active: true,
    urlId
  }).populate(OPTIONS_TO_POPULATE);
  if (!linha) return res.send({ success: false });
  return res.send({ success: true, linha });
});

linhaController.get('/segment/:id', async (req, res) => {
  const { id } = req.params;
  const linha = await Linha.find({ active: true, segment: { id } });
  return res.send({ success: true, linha });
});

linhaController.delete('/:_id', validateAdmin, async (req, res) => {
  const { _id } = req.params;

  Linha.deleteOne({ _id })
    .then(() => res.send({ success: true }))
    .catch(() => res.send({ success: false }));
});

//

linhaController.post('/new', validateAdmin, async (req, res) => {
  const linha = req.body;
  linha.path = cleanText(linha.name);
  linha.urlId = linha.urlId || Math.floor(1000 + Math.random() * 9000);
  try {
    const newLinha = await Linha.create(linha);
    return res.send({ success: true, linha: newLinha });
  } catch (error) {
    return res.send({
      success: false,
      message: JSON.stringify(error),
      code: 'ErrorCreatingLinha'
    });
  }
});

linhaController.put('/', validateAdmin, async (req, res) => {
  const linha = req.body;
  const thisId = linha && linha._id;

  const linhaFound = await Linha.find({ _id: thisId }).limit(1);
  if (!linhaFound.length)
    return res.send({
      success: false,
      message: 'Linha not found',
      code: 'LinhaNotFound'
    });

  // delete linha._id;

  try {
    linha.path = cleanText(linha.name);
    linha.urlId = linha.urlId || Math.floor(1000 + Math.random() * 9000);
    await Linha.updateOne({ _id: thisId }, linha);

    const updatedLinha = await Linha.findOne({ _id: thisId }).populate(
      OPTIONS_TO_POPULATE
    );
    return updateProductsWithSegment({
      linhaId: thisId,
      productList: linha.produtoFinal && linha.produtoFinal.length ? linha.produtoFinal.map(product => {
        if (typeof product === 'string') return product;
        return product._id;
      }) : [],
      amaisList: linha.amaisFinal && linha.amaisFinal.length ? linha.amaisFinal.map(product => {
        if (typeof product === 'string') return product;
        return product._id;
      }) : []
    })
      .then(data => {
        return res.send({
          success: true,
          message: 'Linha updated sucessfully',
          updatedLinha
        });
      })
      .catch(error => {
        throw new Error(error);
      });

    // return res.send({
    //   success: true,
    //   message: 'Linha updated sucessfully',
    //   updatedLinha
    // });
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: 'Error updating linha',
      code: 'LinhaNotUpdated'
    });
  }
});

module.exports = linhaController;
