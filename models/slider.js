const mongoose = require('mongoose')
const Schema = mongoose.Schema

const bannerSchema = new Schema({
  title: String,
  subtext: String,
  img: String,
})

const sliderSchema = new Schema({
  name: { type: String },
  active: { type: Boolean, default: false },
  banner: { type: bannerSchema, default: [] },
  page: [String]
}, {
  timestamps: true
})

module.exports = mongoose.model('slider', sliderSchema)
