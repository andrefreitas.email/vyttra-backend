const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const blogConfigSchema = new Schema(
    {
        bannerPrincipal: String,
        bannerLateral: String,
        destaque: String,
    }
);

module.exports = mongoose.model('blogConfigSchema', blogConfigSchema);
