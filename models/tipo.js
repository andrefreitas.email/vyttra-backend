const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tipoSchema = new Schema({
  name: { type: String, required: true, default: 'Equipamentos' },
  active: { type: Boolean, default: true }
});

module.exports = mongoose.model('tipo', tipoSchema);
