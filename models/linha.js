const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const fileSchema = new Schema({
  name: String,
  path: String
});

const linhaSchema = new Schema({
  name: { type: String, required: true },
  active: { type: Boolean, default: false },
  especifications: { type: String },
  color: { type: String, default: '#fff' },
  urlId: { type: String, required: true },
  path: String,
  img: fileSchema,
  author: { type: Schema.Types.ObjectId, ref: 'admin', required: true },
  tipo: {
    type: Schema.Types.ObjectId,
    ref: 'tipo',
    default: new mongoose.Types.ObjectId()
  },
  segment: {
    type: Schema.Types.ObjectId,
    ref: 'segment',
    default: new mongoose.Types.ObjectId()
  },
  categoria: {
    type: Schema.Types.ObjectId,
    ref: 'categoriaProduct',
    default: new mongoose.Types.ObjectId()
  },
  subCategoria: {
    type: Schema.Types.ObjectId,
    ref: 'subCategoriaProduct',
    default: new mongoose.Types.ObjectId()
  },
  subLinha: {
    type: Schema.Types.ObjectId,
    ref: 'subLinha',
    default: new mongoose.Types.ObjectId()
  },
  produtoFinal: [{ type: Schema.Types.ObjectId, ref: 'produtoFinal' }],
  amaisFinal: [{ type: Schema.Types.ObjectId, ref: 'files' }]
});

module.exports = mongoose.model('linha', linhaSchema);
