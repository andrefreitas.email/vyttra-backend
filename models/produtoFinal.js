const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const fileSchema = new Schema({
  name: String,
  path: String
});

const produtoFinalSchema = new Schema(
  {
    active: { type: Boolean, default: false },
    isReagente: { type: Boolean, default: false },
    name: String,
    img: [String],
    rotina: String,
    isFeatured: Boolean,
    especifications: String,
    moreInfo: String,
    regiao: String,
    catalogoUrl: String,
    detailsOfSegment: String,
    pdf: fileSchema,
    linha: { type: Schema.Types.ObjectId, ref: 'linha' },
    marca: String,
    imgMarca: String,
    url: String,
    shortDesc: String,
    slogan: String,
    mapList: { type: Array, default: [] },
    amais: { type: Schema.Types.Mixed, default: {} }
  },
  {
    timestamps: true
  }
);
produtoFinalSchema.pre('save', async function (next) {
  const self = this;
  const found = await self.constructor.find({ name: this.name }).limit(1);
  if (found.length) next('Name already exists');
  next();
});

module.exports = mongoose.model('produtoFinal', produtoFinalSchema);
