const mongoose = require('mongoose')
const Schema = mongoose.Schema

const subCategoriaProductSchema = new Schema({
  name: { type: String, required: true },
  active: { type: Boolean, default: false }
})

module.exports = mongoose.model('subCategoriaProduct', subCategoriaProductSchema)
