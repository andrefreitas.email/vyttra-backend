const mongoose = require('mongoose')
const Schema = mongoose.Schema

const contentSchema = new Schema({
  page: { type: String },
  data: { type: [{}], default: [] }
}, {
  timestamps: true
})

module.exports = mongoose.model('content', contentSchema)
