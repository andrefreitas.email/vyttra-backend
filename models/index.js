const mongoose = require('mongoose');
if (process.env.NODE_ENV !== 'production') {
  require('custom-env').env(process.env.NODE_ENV);
}
module.exports = {
  mongoConnect: mongoose.connect(process.env.DATABASE_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
};
