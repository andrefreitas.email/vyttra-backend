const mongoose = require('mongoose')
const Schema = mongoose.Schema

const categoryPostSchema = new Schema({
  name: { type: String, required: true },
  active: { type: Boolean, default: false }
})

categoryPostSchema.pre('save', async function(next) {
  const self = this;

  const foundCat = await self.constructor.find({ name: this.name }).limit(1)
  if (foundCat.length) next('Category with this name already exists')

  next()
})

module.exports = mongoose.model('categoryPost', categoryPostSchema)
