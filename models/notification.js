const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const notificationSchema = new Schema({
  name: { type: String, required: true },
  text: { type: String, required: true },
  active: { type: Boolean, default: false }
});

module.exports = mongoose.model('notification', notificationSchema);
