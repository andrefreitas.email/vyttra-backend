const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const Schema = mongoose.Schema;

const fileSchema = new Schema({
  name: String,
  path: String
});

const userSchema = new Schema(
  {
    status: { type: Boolean, ref: 'admin', default: false },
    saudation: { type: String, required: true, default: 'Dr.' },
    name: { type: String, required: true },
    lastname: { type: String, required: true },
    img: fileSchema,
    active: { type: Boolean, default: false },
    cnpj: { type: String, required: true },
    razao: { type: String, required: true },
    tel: { type: String, required: true },
    cel: { type: String, required: true },
    cep: String,
    resetCode: String,
    cidade: { type: String, required: true },
    estado: { type: String, required: true },
    login: { type: String, required: true },
    user: { type: String, required: true },
    registro: fileSchema,
    isNewsletter: Boolean,
    nomeDistribuidor: String,
    cargo: String,
    profissionais: Number,
    email: {
      type: String,
      lowercase: true,
      required: true
    },
    login: String,
    notification: {
      type: Schema.Types.ObjectId,
      ref: 'notification',
      default: new mongoose.Types.ObjectId()
    }
  },
  {
    timestamps: true
  }
);

userSchema.pre('save', async function(next) {
  const self = this;

  const foundEmail = await self.constructor.findOne({ email: this.email });
  if (foundEmail) return next('Email already exists');
  next();
});

userSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('user', userSchema);
