const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const emailSchema = new Schema({
  email: String,
  admin: { type: Boolean, default: false },
  type: { type: String, default: 'user'},
});

module.exports = mongoose.model('email', emailSchema);
