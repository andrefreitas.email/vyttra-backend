const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const filesSchema = new Schema({
  name: { type: String, required: true },
  active: { type: Boolean, default: true },
  type: String,
  url: { type: String, default: '' },
  linha: { type: Schema.Types.ObjectId, ref: 'linha' },
  content: { type: Schema.Types.Mixed }
});

module.exports = mongoose.model('files', filesSchema);
