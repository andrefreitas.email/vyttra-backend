const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const imgSchema = new Schema({
  name: String,
  path: String
});

const postSchema = new Schema(
  {
    name: String,
    img: imgSchema,
    active: { type: Boolean, default: true },
    isFeatured: { type: Boolean, default: false },
    url: String,
    content: String,
    author: { type: Schema.Types.ObjectId, ref: 'admin', required: true },
    category: { type: Schema.Types.ObjectId, ref: 'categoryPost' },
    shortDesc: { type: String, default: '' }
  },
  {
    timestamps: true
  }
);

postSchema.pre('save', async function (next) {
  const self = this;
  const foundEmail = await self.constructor.find({ name: this.name }).limit(1);
  if (foundEmail.length) next('Posts name already exists');
  next();
});

module.exports = mongoose.model('post', postSchema);
