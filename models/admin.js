const mongoose = require('mongoose')
const Schema = mongoose.Schema

const adminSchema = new Schema({
  name: { type: String },
  img: [String],
  active: { type: Boolean, default: true },
  email: {
    type: String,
    lowercase: true,
    required: true
  },
  login: { type: String }
}, {
    timestamps: true
  })

adminSchema.pre('save', async function(next) {
  const self = this;
  const foundEmail = await self.constructor.find({ email: this.email }).limit(1)
  if (foundEmail.length) next('Email already exists')
  next()
})

module.exports = mongoose.model('admin', adminSchema)
