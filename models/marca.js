const mongoose = require('mongoose')
const Schema = mongoose.Schema

const marcaSchema = new Schema({
  name: { type: String, required: true },
  active: { type: Boolean, default: false }
})

module.exports = mongoose.model('marca', marcaSchema)
