const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const contatoSchema = new Schema({
  type: { type: String, required: true },
  respondido: { type: Boolean, default: false },
  dados: { type: Schema.Types.Mixed }
});

module.exports = mongoose.model('contato', contatoSchema);
